//
//  GeneralPresenter.swift
//  DemoApp
//
//  Created by Kakhi Kiknadze on 3/9/19.
//  Copyright © 2019 Kakhi Kiknadze. All rights reserved.
//

import UIKit

class GeneralPresenter: UINavigationController {
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.isNavigationBarHidden = true
    }
    
    override func dismiss(animated: Bool = true, completion: (() -> Void)? = nil)
    {
        super.dismiss(animated: animated, completion: completion)
    }
    
}
