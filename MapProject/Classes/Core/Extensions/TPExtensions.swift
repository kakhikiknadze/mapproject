//
//  TBExtensions.swift
//  TBCPay
//
//  Created by Saba Kobaidze on 4/26/18.
//  Copyright © 2018 Saba Kobaidze. All rights reserved.
//

import Foundation
import RDExtensionsSwift
import SVProgressHUD

precedencegroup UnwrappedAssign {
    associativity: right
}
infix operator ?= : UnwrappedAssign

// TODO: Change "newValue: Any?" to "newValue: T?"
func ?=<T : Any>(base: inout T, newValue: Any?)
{
    if newValue != nil,
        let nv = newValue as? T
    {
        base = nv
    }
}

extension CALayer {
    func applySketchShadow(
        color: UIColor = .black,
        alpha: Float = 0.5,
        x: CGFloat = 0,
        y: CGFloat = 2,
        blur: CGFloat = 4,
        spread: CGFloat = 0)
    {
        shadowColor = color.cgColor
        shadowOpacity = alpha
        shadowOffset = CGSize(width: x, height: y)
        shadowRadius = blur / 2.0
        if spread == 0 {
            shadowPath = nil
        } else {
            let dx = -spread
            let rect = bounds.insetBy(dx: dx, dy: dx)
            shadowPath = UIBezierPath(rect: rect).cgPath
        }
    }
    
    
    @IBInspectable var roundedCorners : Bool { get { return self.cornerRadius == self.frame.height/2 } set { if(newValue) { self.cornerRadius = self.frame.height/2; self.masksToBounds = true } } }
    
}

extension CGRect {
    
    /// RDExtensionsSwift: Get or Set frame width
    var width : CGFloat { get { return self.size.width } set { self.size = CGSize(width: newValue, height: self.size.height) } }
    
    /// RDExtensionsSwift: Get or Set frame height
    var height : CGFloat { get { return self.size.height } set { self.size = CGSize(width: self.size.width, height: newValue) } }
    
}

extension NSLayoutConstraint {
    @IBInspectable var shouldAdjustConstantToFitDevice : Bool { get { return false } set { if(newValue) { self.constant *= Constants.ScreenFactor } } }
}

extension UIViewController {
    
    public class func loadFromStoryboard() -> Self
    {
        return self.load(from: self.className.replacingOccurrences(of: "NavigationController", with: "").replacingOccurrences(of: "ViewController", with: "").replacingOccurrences(of: "Controller", with: ""))!
    }
    
    public static func loadAsRootViewController() -> Self
    {
        return self.loadFromStoryboard().loadAsRootViewController()!
    }
    
    func addTapGestureRecognizer(with selector: Selector)
    {
        self.view.addTapGestureRecognizer(for: self, with: selector)
    }
    
    //    var standardFailBlock: Network.FailBlock
    //    {
    //        return self.view.standardFailBlock
    //    }
    
    func startLoader()
    {
        self.view.startLoader()
    }
    
    func stopLoader()
    {
        self.view.stopLoader()
    }
    
    var lastPresentedViewController : UIViewController?
    {
        get
        {
            var top: UIViewController? = self
            while true
            {
                if let presented = top?.presentedViewController
                {
                    top = presented
                }
                else if let nav = top as? UINavigationController
                {
                    top = nav.visibleViewController
                }
                else if let tab = top as? UITabBarController
                {
                    top = tab.selectedViewController
                }
                else
                {
                    break
                }
            }
            return top
        }
    }
    
    func present(on viewController: UIViewController, animated: Bool = true, completion: (() -> Void)? = nil)
    {
        viewController.present(self, animated: animated, completion: completion)
    }
    
}

extension UIView {
    
    @IBInspectable var borderWidth : CGFloat { get { return self.layer.borderWidth } set { self.layer.borderWidth = newValue } }
    
    @IBInspectable var borderColor : UIColor? { get { return self.layer.borderColor != nil ? UIColor(cgColor: self.layer.borderColor!) : nil } set { self.layer.borderColor = newValue?.cgColor } }
    
    @IBInspectable var cornerRadius : CGFloat { get { return self.layer.cornerRadius } set { self.layer.cornerRadius = newValue; self.clipsToBounds = true } }
    
    @IBInspectable var adjustCornerRadiusToFitDevice : Bool { get { return false } set { if(newValue) { self.layer.cornerRadius = self.cornerRadius * Constants.ScreenFactor } } }
    
    @IBInspectable var roundedCorners : Bool { get { return self.layer.cornerRadius == self.frame.height/2 } set { if(newValue) { self.layer.cornerRadius = self.frame.height/2; self.clipsToBounds = true } } }
    
    @IBInspectable var shadowColor : UIColor? { get { return self.layer.shadowColor != nil ? UIColor(cgColor: self.layer.shadowColor!) : nil } set { self.layer.shadowColor = newValue?.cgColor } }
    
    @IBInspectable var shadowOpacity : Float { get { return self.layer.shadowOpacity } set { self.layer.shadowOpacity = newValue } }
    
    @IBInspectable var shadowRadius: CGFloat { get { return self.layer.shadowRadius } set { self.layer.shadowRadius = newValue } }
    
    @IBInspectable var shadowOffset : CGSize { get { return self.layer.shadowOffset } set { self.layer.shadowOffset = newValue } }
    
    @IBInspectable var masksToBounds : Bool { get { return self.layer.masksToBounds } set { self.layer.masksToBounds = newValue } }
    
    @IBInspectable var shouldRasterize : Bool { get { return self.layer.shouldRasterize } set { self.layer.shouldRasterize = newValue } }
    
    func addTapGestureRecognizer(for observer: UIViewController, with selector: Selector)
    {
        self.addGestureRecognizer(UITapGestureRecognizer(target: observer, action: selector))
    }
    
    private static func _loadFromNib<T>(owner: Any? = nil, with options: [AnyHashable : Any]? = nil) -> T
    {
        return self.load(with: self.className, for: owner, with: options as! [UINib.OptionsKey : Any])! as! T
    }
    
    static func loadFromNib(owner: Any? = nil, with options: [AnyHashable : Any]? = nil) -> Self
    {
        return self._loadFromNib(owner: owner, with: options)
    }
    
    func drawDashedLine(start p0: CGPoint, end p1: CGPoint, color: CGColor) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 1 * Constants.ScreenFactor
        shapeLayer.lineDashPattern = [2 * Constants.ScreenFactor, 2 * Constants.ScreenFactor] as [NSNumber]
        
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        self.layer.addSublayer(shapeLayer)
    }
    
    func mask(with rect: CGRect, inverse: Bool = false)
    {
        let path = UIBezierPath(rect: rect)
        let maskLayer = CAShapeLayer()
        if(inverse)
        {
            path.append(UIBezierPath(rect: self.bounds))
            maskLayer.fillRule = CAShapeLayerFillRule.evenOdd
        }
        maskLayer.path = path.cgPath
        self.layer.mask = maskLayer
    }
    
    func round(corners: UIRectCorner, radius: CGFloat)
    {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    //    func animate(alpha: CGFloat, duration: TimeInterval = Constants.Animation.DefaultAnimationDuration, delay: TimeInterval = 0, options: UIView.AnimationOptions = [], completion: ((Bool) -> Void)? = nil)
    //    {
    //        UIView.animate(withDuration: duration, delay: delay, options: options, animations: { [weak self] in
    //            self?.alpha = alpha
    //        }, completion: completion)
    //    }
    
    func startLoader()
    {
        SVProgressHUD.show()
        SVProgressHUD.setDefaultMaskType(.gradient)
    }
    
    func stopLoader()
    {
        SVProgressHUD.dismiss()
        SVProgressHUD.setDefaultMaskType(.none)
    }
    
    //    var standardFailBlock: Network.FailBlock
    //    {
    //        return { [weak self] status in
    //            self?.stopLoader()
    //            UIApplication.shared.keyWindow?.lastPresentedViewController?.present(UIAlertController(message: status.localizedDescription, actions: [], cancelButtonTitle: LocalString("Close")), animated: true, completion: nil)
    //        }
    //    }
    
    func subviews<T>(ofKind type: T.Type, recursively: Bool = false) -> [T]
    {
        var arraySubviews : [T] = []
        for view in self.subviews
        {
            if let view = view as? T
            {
                arraySubviews.append(view)
            }
            
            if(recursively)
            {
                arraySubviews += view.subviews(ofKind: type, recursively: recursively)
            }
        }
        return arraySubviews
    }
    
}

extension UIButton {
    
    @IBInspectable var shouldAdjustFontSizeToFitDevice : Bool { get { return false } set { if(newValue) { self.adjustsFontSizeToFitDevice() } } }
    
    @IBInspectable var numberOfLines: Int { get { return self.titleLabel?.numberOfLines ?? 0 } set { self.titleLabel?.numberOfLines = newValue } }
    
    @IBInspectable var adjustContentEdgeInsetsToFitDevice : Bool
        {
        get { return false }
        set
        {
            self.contentEdgeInsets.top *= Constants.ScreenFactor
            self.contentEdgeInsets.left *= Constants.ScreenFactor
            self.contentEdgeInsets.bottom *= Constants.ScreenFactor
            self.contentEdgeInsets.right *= Constants.ScreenFactor
        }
    }
    
    @IBInspectable var adjustImageEdgeInsetsToFitDevice : Bool
        {
        get { return false }
        set
        {
            self.imageEdgeInsets.top *= Constants.ScreenFactor
            self.imageEdgeInsets.left *= Constants.ScreenFactor
            self.imageEdgeInsets.bottom *= Constants.ScreenFactor
            self.imageEdgeInsets.right *= Constants.ScreenFactor
        }
    }
    
    @IBInspectable var adjustTitleEdgeInsetsToFitDevice : Bool
        {
        get { return false }
        set
        {
            self.titleEdgeInsets.top *= Constants.ScreenFactor
            self.titleEdgeInsets.left *= Constants.ScreenFactor
            self.titleEdgeInsets.bottom *= Constants.ScreenFactor
            self.titleEdgeInsets.right *= Constants.ScreenFactor
        }
    }
    
    func adjustsFontSizeToFitDevice()
    {
        self.titleLabel?.adjustsFontSizeToFitDevice()
    }
    
    func alignTextBelow(spacing: CGFloat = 6.0) {
        if let image = self.imageView?.image {
            let imageSize: CGSize = image.size
            self.titleEdgeInsets = UIEdgeInsets(top: imageSize.height + spacing, left: -imageSize.width, bottom: -(imageSize.height), right: 0.0)
            let labelString = NSString(string: self.titleLabel!.text!)
            let titleSize = labelString.size(withAttributes: [NSAttributedString.Key.font: self.titleLabel!.font])
            self.imageEdgeInsets = UIEdgeInsets(top: -(titleSize.height + spacing), left: 0.0, bottom: 0.0, right: -titleSize.width)
        }
    }
    
    func centerImageAndButton(_ gap: CGFloat, imageOnTop: Bool) {
        
        guard let imageView = self.currentImage,
            let titleLabel = self.titleLabel?.text else { return }
        
        let sign: CGFloat = imageOnTop ? 1 : -1
        self.titleEdgeInsets = UIEdgeInsets(top: (imageView.size.height + gap) * sign, left: -imageView.size.width, bottom: 0, right: 0);
        
        let titleSize = titleLabel.size(withAttributes:[NSAttributedString.Key.font: self.titleLabel!.font!])
        self.imageEdgeInsets = UIEdgeInsets(top: -(titleSize.height + gap) * sign, left: -8.0, bottom: 0, right: -titleSize.width)
    }
    
    func underline()
    {
        let attributedText = NSMutableAttributedString(string: self.title(for: .normal) ?? .empty)
        attributedText.addAttributes([
            .font                  :        self.titleLabel!.font,
            .foregroundColor       :        self.titleLabel!.textColor,
            .underlineStyle        :        NSUnderlineStyle.single.rawValue
            ], range: NSRange(location: 0, length: attributedText.length))
        self.setAttributedTitle(attributedText, for: .normal)
    }
    
    /// RDExtensionsSwift: Sets attributed text with given components
    func setAttributedText(_ components: [(String, [NSAttributedString.Key: Any])], for state: UIControl.State)
    {
        let label = UILabel()
        label.setAttributedText(components)
        self.setAttributedTitle(label.attributedText, for: state)
    }
    
}

extension UIToolbar {
    
    func ToolbarPicker(target: Any?, mySelect : Selector) -> UIToolbar {
        
        let toolBar = UIToolbar()
        
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.blue
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: target, action: mySelect)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        return toolBar
    }
    
}

extension UILabel {
    
    @IBInspectable var shouldAdjustFontSizeToFitDevice : Bool { get { return false } set { if(newValue) { self.adjustsFontSizeToFitDevice() } } }
    
    func adjustsFontSizeToFitDevice()
    {
        if let f = self.font
        {
            self.font = UIFont(name: f.fontName, size: f.pointSize * Constants.ScreenFactor)
        }
    }
    
    @IBInspectable var paragraphLineSpacing: CGFloat
        {
        get
        {
            let attribute = self.attributedText?.attribute(.paragraphStyle, at: 0, longestEffectiveRange: nil, in: NSRange(location: 0, length: self.string.count)) as? NSParagraphStyle
            return attribute?.lineSpacing ?? 0
        }
        set
        {
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = newValue
            paragraphStyle.alignment = self.textAlignment
            self.attributedText = NSAttributedString(string: self.string, attributes: [.paragraphStyle
                : paragraphStyle])
        }
    }
    
    @IBInspectable var shouldAdjustParagraphLineSpacingToFitDevice : Bool { get { return false } set { if(newValue) { self.paragraphLineSpacing *= Constants.ScreenFactor } } }
    
    /// RDExtensionsSwift: Sets attributed text with given components
    func setAttributedText(_ components: [(String, [NSAttributedString.Key: Any])])
    {
        let attributedText = NSMutableAttributedString(string: components.map { $0.0 }.joined())
        for i in 0 ..< components.count
        {
            attributedText.addAttributes(components[i].1, range: NSRange(location: components[0 ..< i].map { $0.0 }.joined().length, length: components[i].0.length))
        }
        self.attributedText = attributedText
    }
    
    func boldFirstWord()
    {
        let textComponents = self.string.components(separatedBy: String.space)
        let textOtherPart = String.space + textComponents.dropFirst().joined()
        if textComponents.count >= 2,
            let textBoldPart = textComponents.first,
            let font = self.font,
            let fontBaseName = font.fontName.components(separatedBy: "-").first,
            let boldFont = UIFont(name: fontBaseName + "-Bold", size: font.pointSize),
            let otherFont = UIFont(name: font.fontName, size: font.pointSize - 2)
        {
            self.setAttributedText([
                (textBoldPart, [NSAttributedString.Key.font: boldFont]),
                (textOtherPart, [NSAttributedString.Key.font: otherFont])
                ])
        }
    }
    
}

extension UITextField {
    
    @IBInspectable var placeHolderColor: UIColor? {
        get { return self.placeHolderColor } set { if (newValue != nil) {
            let attributedString = NSMutableAttributedString(attributedString: self.attributedPlaceholder ?? NSAttributedString())
            attributedString.addAttributes([NSAttributedString.Key.foregroundColor: newValue!], range: NSRange(location: 0, length: attributedString.length))
            self.attributedPlaceholder = attributedString
            }
        }
    }
    
    @IBInspectable var shouldAdjustFontSizeToFitDevice : Bool { get { return false } set { if(newValue) { self.adjustsFontSizeToFitDevice() } } }
    
    func adjustsFontSizeToFitDevice()
    {
        if let f = self.font
        {
            self.font = UIFont(name: f.fontName, size: f.pointSize * Constants.ScreenFactor)
        }
    }
    
    /// RDExtensionsSwift: Sets attributed text with given components
    func setAttributedText(_ components: [(String, [NSAttributedString.Key: Any])], for state: UIControl.State)
    {
        let label = UILabel()
        label.setAttributedText(components)
        self.attributedText = label.attributedText
    }
    
    func removePasswordSuggestion() {
        if #available(iOS 12, *) {
            self.textContentType = .oneTimeCode
        } else {
            self.textContentType = .init(rawValue: "")
        }
    }
    
}

extension UIImage {
    
    func tint(with color: UIColor) -> UIImage?
    {
        if let maskImage = self.cgImage
        {
            let width = self.size.width
            let height = self.size.height
            let bounds = CGRect(x: 0, y: 0, width: width, height: height)
            let colorSpace = CGColorSpaceCreateDeviceRGB()
            let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
            if let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)
            {
                context.clip(to: bounds, mask: maskImage)
                context.setFillColor(color.cgColor)
                context.fill(bounds)
                
                if let cgImage = context.makeImage()
                {
                    let coloredImage = UIImage(cgImage: cgImage)
                    return coloredImage
                }
            }
        }
        return nil
    }
}

protocol EnumCollection : Hashable {
}

extension EnumCollection {
    
    static var items : Array<Self>
    {
        typealias S = Self
        return Array(AnySequence { () -> AnyIterator<S> in
            var raw = 0
            return AnyIterator {
                let current : Self = withUnsafePointer(to: &raw) { $0.withMemoryRebound(to: S.self, capacity: 1) { $0.pointee } }
                guard current.hashValue == raw else { return nil }
                raw += 1
                return current
            }
        })
    }
    
}

extension String {
    
    var toCharacter: Character { get { return Character(self) } }
    
    var isNumber : Bool {
        get{
            return !isEmpty && rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
        }
    }
    
    func trunc(length: Int, trailing: String = "…") -> String {
        return (self.count > length) ? self.prefix(length) + trailing : self
    }
    
    
    func substring(from leftIndex: Int, length rightIndex: Int) -> String
    {
        return self[NSMakeRange(leftIndex, rightIndex)]
    }
    
    static var empty: String { get { return "" } }
    
    static var space: String { get { return " " } }
    
    static var dot: String { get { return "." } }
    
    static var comma: String { get { return String(",") } }
    
    static var newLine: String { get { return String("\n") } }
    
}

extension Date {
    
    var timeIntervalSince1970Milliseconds : Int64 { get { return (self.timeIntervalSince1970 * 1000).toInt64 } }
    
    func toString(_ format: String, locale: Locale? = nil, timeZone: TimeZone? = nil) -> String
    {
        let df = DateFormatter()
        df.dateFormat = format
        if let locale = locale
        {
            df.locale = locale
        }
        if let tz = timeZone
        {
            df.timeZone = tz
        }
        return df.string(from: self)
    }
    
    init?(formattedString: String, format: String, locale: Locale? = nil, timeZone: TimeZone? = nil)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        if let locale = locale
        {
            dateFormatter.locale = locale
        }
        if let timeZone = timeZone
        {
            dateFormatter.timeZone = timeZone
        }
        if let date = dateFormatter.date(from: formattedString)
        {
            self = date
        }
        else
        {
            return nil
        }
    }
    
    init(timeIntervalSince1970Milliseconds: Int64)
    {
        self = Date(timeIntervalSince1970: timeIntervalSince1970Milliseconds.toDouble / 1000)
    }
    
}

extension String {
    
    public func fontSizeFor(width: CGFloat, height: CGFloat, fontName: String) -> CGFloat
    {
        var fontSize = 0 as CGFloat
        var w = 0 as CGFloat
        while(w < width)
        {
            w = self.widthForHeight(height, font: UIFont(name: fontName, size: fontSize)!)
            fontSize += 1
        }
        return fontSize - 1
    }
    
    public func attributedFontSizeFor(width: CGFloat, height: CGFloat, fontName: String) -> CGFloat
    {
        var fontSize = 0 as CGFloat
        var w = 0 as CGFloat
        while(w < width)
        {
            fontSize += 1
            let textView = UITextView(frame: CGRect(x: 0, y: 0, width: 0, height: height))
            textView.text = self
            textView.font = UIFont(name: fontName, size: fontSize)
            textView.sizeToFit()
            w = textView.frame.width
        }
        return fontSize - 1
    }
    
    func toHtmlAttributedText(encoding: Encoding = .utf8, options: [NSAttributedString.DocumentReadingOptionKey : Any] = [:]) -> NSAttributedString?
    {
        if let data = data(using: encoding)
        {
            var _options: [NSAttributedString.DocumentReadingOptionKey : Any] = [
                .documentType               :       NSAttributedString.DocumentType.html,
                .characterEncoding          :       encoding.rawValue
            ]
            for option in options
            {
                _options[option.key] = option.value
            }
            do
            {
                return try NSAttributedString(data: data, options: _options, documentAttributes: nil)
            }
            catch
            {
                return nil
            }
        }
        return nil
    }
    
    var htmlToString: String? { get { return self.toHtmlAttributedText()?.string } }
    
}

extension NSAttributedString {
    
    /// RDExtensionsSwift: Calculate and return width of the NSAttributedString for given hight with given font
    public func widthForHeight(_ height: CGFloat) -> CGFloat
    {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], context: nil)
        return ceil(boundingBox.width)
    }
    
    /// RDExtensionsSwift: Calculate and return height of the NSAttributedString for given width with given font
    public func heightForWidth(_ width: CGFloat) -> CGFloat
    {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], context: nil)
        return ceil(boundingBox.height)
    }
    
}

extension UIWindow
{
    var lastPresentedViewController : UIViewController?
    {
        get
        {
            return self.rootViewController?.lastPresentedViewController
        }
    }
}

protocol Reusable: class {
    static var reuseIdentifier: String { get }
}

extension Reusable {
    static var reuseIdentifier: String { return String(describing: self) }
}

extension NSNumber {
    
    convenience init(value: CGFloat)
    {
        self.init(value: value.toDouble)
    }
    
}

extension UITableView {
    
    @IBInspectable var adjustSeparatorInsetsToFitDevice: Bool { get { return false } set { if(newValue) { self.separatorInset = UIEdgeInsets(top: self.separatorInset.top * Constants.ScreenFactor, left: self.separatorInset.left * Constants.ScreenFactor, bottom: self.separatorInset.bottom * Constants.ScreenFactor, right: self.separatorInset.right * Constants.ScreenFactor) } } }
    
    func dequeueReusableCell <T: UITableViewCell> (with indexPath: IndexPath) -> T where T: Reusable {
        guard let cell = self.dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath as IndexPath) as? T else {
            fatalError("Could Not Dequeue Cell With Identifier: \(T.reuseIdentifier)")
        }
        return cell
    }
    
    func dequeueReusableHeaderFooterView<T: UITableViewHeaderFooterView>() -> T? where T: Reusable {
        return self.dequeueReusableHeaderFooterView(withIdentifier: T.reuseIdentifier) as! T?
    }
    
    func isLast(section: Int) -> Bool
    {
        return section == self.numberOfSections - 1
    }
    
    func isLast(row: Int, inSection section: Int) -> Bool
    {
        return row == self.numberOfRows(inSection: section) - 1
    }
    
    func isLast(indexPath: IndexPath) -> Bool
    {
        return self.isLast(section: indexPath.section) && self.isLast(row: indexPath.row, inSection: indexPath.section)
    }
    
}

extension UICollectionView {
    func dequeueReusableCell <T: UICollectionViewCell> (with indexPath: IndexPath) -> T where T: Reusable {
        guard let cell = self.dequeueReusableCell(withReuseIdentifier: T.reuseIdentifier, for: indexPath as IndexPath) as? T else {
            print("Could not dequeue cell with identifier :\(T.reuseIdentifier)")
            return T()
        }
        return cell
    }
    
    func dequeueReusableSupplementaryView<T: UICollectionViewCell>(elementKind: String, indexPath: IndexPath) -> T where T: Reusable {
        return self.dequeueReusableSupplementaryView(ofKind: elementKind, withReuseIdentifier: T.reuseIdentifier, for: indexPath) as! T
    }
}

public extension Collection where Index == Int, Iterator.Element : Collection, Iterator.Element.Index == Int {
    
    /// RDExtensionsSwift: Return element at indexPath
    subscript(indexPath: IndexPath) -> Iterator.Element.Iterator.Element?
    {
        get
        {
            return self.object(at: indexPath)
        }
        set
        {
            self.set(object: newValue, at: indexPath)
        }
    }
    
    /// RDExtensionsSwift: Return element at indexPath
    @discardableResult mutating func set(object: Iterator.Element.Iterator.Element?, at indexPath: IndexPath) -> Bool
    {
        if let object = object,
            indexPath.section < Array(self).count && indexPath.row < Array(self[indexPath.section]).count
        {
            //            self[indexPath.section][indexPath.row] = object
            var items: [[Iterator.Element.Iterator.Element]] = []
            for i in 0 ..< indexPath.section
            {
                items.append(self[i] as! [Iterator.Element.Iterator.Element])
            }
            var newItems: Array<Element.Element> = []
            for i in 0 ..< indexPath.row
            {
                newItems.append(self[indexPath.section][i])
            }
            newItems.append(object)
            for i in indexPath.row + 1 ..< self[indexPath.section].count
            {
                newItems.append(self[indexPath.section][i])
            }
            items.append(newItems)
            for i in indexPath.section + 1 ..< self.count
            {
                items.append(self[i] as! [Iterator.Element.Iterator.Element])
            }
            self = items as! Self
            return true
        }
        return false
    }
    
}

extension Substring {
    
    var toString: String { get { return String(self) } }
    
}

extension UISearchBar {
    
    var string: String { get { return self.text ?? .empty } set { self.text = newValue } }
    
}

extension Collection {
    
    func indexes(_ isIncluded: @escaping (Element) -> Bool) -> [Int]
    {
        var indexes: [Int] = []
        var index = 0
        for element in self
        {
            if(isIncluded(element))
            {
                indexes.append(index)
            }
            index += 1
        }
        return indexes
    }
    
    func enumeratedMap<T>(transform: @escaping (Element, Int) -> T) -> [T]
    {
        var items: [T] = []
        var index = 0
        for item in self
        {
            items.append(transform(item, index))
            index += 1
        }
        return items
    }
    
    func iterableForEach(_ body: @escaping (Element) -> Void)
    {
        self.iterableForEach { item, _ in
            body(item)
        }
    }
    
    func iterableForEach(_ body: @escaping (Element, Int) -> Void)
    {
        var index = 0
        for item in self
        {
            body(item, index)
            index += 1
        }
    }
    
}

extension Collection where Element: Hashable {
    
    func uniques() -> [Element]
    {
        var elements: [Element] = []
        self.iterableForEach { element in
            if(!elements.contains(element))
            {
                elements.append(element)
            }
        }
        return elements
    }
    
}

extension UIDevice {
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        return identifier
    }
    
    var isIphoneXFamily: Bool {
        return self.modelName.contains("iPhone10,3") || self.modelName.contains("iPhone10,6") || self.modelName.contains("iPhone11")
    }
}

extension Double {
    func rounded(toPlaces places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

