//
//  Constants.swift
//  DemoApp
//
//  Created by Kakhi Kiknadze on 3/9/19.
//  Copyright © 2019 Kakhi Kiknadze. All rights reserved.
//

import UIKit

class Constants {
    
    static let ScreenFactor = UIScreen.main.bounds.width/320
    
    class Network {
        static let APIUrl = "https://api.opendota.com/api"
    }
    
    class Regex {
        
        static let Email = "([A-Z0-9a-z._%+-]{2,})+@([A-Za-z0-9.-]{2,})+\\.[A-Za-z]{2,4}"
        static let MobileNumber = "^5[0-9]{8}"
        static let MobileNumberTyping = "^[0-9]{0,9}"
        static let Password = "((?=.*\\d)(?=.*[A-Z])(?=.*[a-z])\\w.{8,20}\\w)"
        static let SmsCode = "^[0-9]{4}"
        static let PersonalId = "^[0-9]{11}"
        static let PersonalIdTyping = "^[0-9]{0,11}"
        static let AnyValue = ".*"
        static let Numbers = "[0-9]*"
        static let Currency = "^[0-9]*(|\\.[0-9]{1,2})$"
        static let CurrencyTyping = "^[0-9]*(|\\.[0-9]{0,2})$"
        static let TPCurrencyTyping = "^((0|0([1-9][0-9]{0,3}))|([1-9]([0-9]{0,4})))(|\\.[0-9]{0,2})$"
        static let FebruaryMonthDays = "^([1-9]{1}|1[0-9]|2[0-8])$"
        static let CardMask = "[0-9]{4}"
        static let CardMaskTyping = "[0-9]{0,4}"
        
    }
    
}
